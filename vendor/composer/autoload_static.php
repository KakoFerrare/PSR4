<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitbc5c61df3b659fd80c496650e4349dba
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'Aplicacao\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Aplicacao\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/Aplicacao',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitbc5c61df3b659fd80c496650e4349dba::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitbc5c61df3b659fd80c496650e4349dba::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
